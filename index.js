const fs = require('fs');
const axios = require('axios');

const levels = {
  'Débutant': 1,
  'Intermédiaire': 2,
  'Avancé': 3,
  Courant: 4,
  'Langue maternelle': 5,
};

const transform = async (input) => {
  return {
    'id': input.id,
    "firstname": input.firstname,
    "lastname": input.lastname,
    "dob": input.birthday,
    "address": {
      "zipCode": input.zipCode,
      "street": input.street,
      "city": input.city,
      "countryCode": "FR"
    },
    "experiences": input.experiences.map(exp => {
      exp.jobId = exp.job.id;
      delete exp.job;
      return exp;
    }),
    "certificates": input.certificates.map(cer => {
      cer.certificate = cer.certificate.title,
        cer.type = cer.certificateType.title;
      delete cer.certificateType;
      return cer;
    }),
    "languages": input.languages.map((lan) => {
      t = lan.title, l = lan.level;
      lan.languageId = lan.id;
      delete lan.id;
      delete lan.title;
      lan.title = t;
      delete lan.level;
      lan.levelTitle = l;
      lan.level = levels[l];
      return lan;
    })
  };
};

(async () => {
  const inStr = fs.readFileSync('./in.json', 'UTF-8');
  const jsonIn = JSON.parse(inStr);
  const output = await transform(jsonIn);
  const outStr = JSON.stringify(output, null, 2);
  fs.writeFileSync('./out.json', outStr);
})();